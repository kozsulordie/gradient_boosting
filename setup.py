from os import path
from setuptools import setup, find_packages
from gradient_boosting import __version__


directory = path.dirname(path.abspath(__file__))
with open(path.join(directory, 'requirements.txt')) as f:
    required = f.read().splitlines()


setup(
    name='gradient_boosting',
    version=__version__,
    description='Gradient Boosting wrapper',
    packages=find_packages(),
    author='Ferran Muiños',
    author_email='ferran.muinos@irbbarcelona.org',
    install_requires=required,
    license='GNU GPLv3',
    url='http://bitbucket.org/ferran_muinos/gradient_boosting',
)
