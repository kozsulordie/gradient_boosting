import pandas as pd
import numpy as np
import xgboost as xgb
import matplotlib.pyplot as plt
from collections import defaultdict
from sklearn.metrics import accuracy_score, matthews_corrcoef
from sklearn.metrics import explained_variance_score, mean_squared_error, r2_score
from gradient_boosting.mutual_information import generate_feature_ranking

color_list = ['sandybrown', 'firebrick', 'red', 'darkseagreen', 'darkgray', 'violet', 'steelblue', 'tomato']


# TODO: write a function or a class that provides with the meta-optimization
# TODO: by doing the Bayesian optimization approach e.g. in scikit-optimize


class BoostingBase(object):


    def __init__(self, **params):

        self.model = None
        self.params = params
        self.x_data = None
        self.y_data = None
        self.feat_imp = {}
        self.feat_imp_std = {}
        self.feat_dep = {}
        self.cv_result = None


    def train(self, x_data, y_data):

        raise NotImplementedError


    def predict(self, x_data):

        raise NotImplementedError


    def get_estimators(self):

        raise NotImplementedError


    def get_default_score(self):

        raise NotImplementedError


    @staticmethod
    def get_estimator_importance(tree, imp_type):
        """
        Args:
            tree: str: produced by method get_dump
            imp_type: str: importance type
        Returns:
            dict: for each feature id (fid) provides
                  average imp_type score of fid in tree
        """
        feat_importances = {}
        scores = defaultdict(list)
        # for best understanding of the following piece of code
        # consider the format given by the estimators resulting
        # from xgboost get_dump() method
        for line in tree.split('\n'):
            # look for the opening square bracket
            arr = line.split('[')
            # if no opening bracket (leaf node), ignore this line
            if len(arr) == 1:
                continue
            # look for the closing bracket, extract only info within that bracket
            fid = arr[1].split(']')
            # extract gain or cover from string after closing bracket
            g = float(fid[1].split(imp_type + '=')[1].split(',')[0])
            # extract feature name from string before closing bracket
            fid = fid[0].split('<')[0]
            scores[fid].append(g)
        return scores


    def get_importance(self, **kwargs):
        """
        Gets standard deviation of each feature importance by considering
        all the feature importances computed for all the estimators in the model
        """
        for imp_type in ['cover', 'gain']:
            all_feat_importances = defaultdict(list)
            for tree in self.get_estimators():
                feat_importances = self.get_estimator_importance(tree, imp_type)
                for k, v in feat_importances.items():
                    all_feat_importances[k] += v
            for k, v in all_feat_importances.items():
                q_5 = np.percentile(v, 5)
                q_95 = np.percentile(v, 95)
                v = np.array(v)
                all_feat_importances[k] = v[(v > q_5) & (v < q_95)]
            self.feat_imp[imp_type] = {k: np.mean(np.array(v)) for k, v in all_feat_importances.items()}
            self.feat_imp_std[imp_type] = {k: np.std(np.array(v)) for k, v in all_feat_importances.items()}
        for imp_type in ['permutation']:
            self.feat_imp['permutation'] = {}
            self.feat_imp_std['permutation'] = {}
            self.permutation_importance(**kwargs)


    @staticmethod
    def permutation(fid, x_data):
        """
        Args:
            fid: str: feature ID
            x_data: pandas dataframe: covariate
        Returns:
            x_data with shuffled fid values
        """

        assert(fid in x_data.columns.values)
        df = x_data.copy()
        df[fid] = np.random.permutation(df[fid])
        return df


    def permutation_importance(self, **kwargs):
        """
        Feature Importance Analysis via Permutation Test
        """
        # reference: https://medium.com/@ceshine/feature-importance-measures-for-tree-models-part-i-47f187c1a2c3
        # reference: http://parrt.cs.usfca.edu/doc/rf-importance/index.html

        v = []
        main_score = self.score(self.x_data, self.y_data, **kwargs)
        for c in self.x_data.columns.values:
            permuted_x_data = self.permutation(c, self.x_data)
            self.feat_imp['permutation'][c] = main_score - self.score(permuted_x_data, self.y_data, **kwargs)
            self.feat_imp_std['permutation'][c] = 0


    def partial_dependence(self, feat, grid_size=50):

        raise NotImplementedError


    def add_partial_dependence(self, feat_iterable, **kwargs):

        for feat in feat_iterable:
            if feat not in self.feat_dep:
                grid, y_pred = self.partial_dependence(feat, **kwargs)
                self.feat_dep.update({feat: (grid, y_pred)})


    def plot_importance(self, imp_type, ax, std_kwarg=True):
        """
        reference: https://medium.com/@ceshine/feature-importance-measures-for-tree-models-part-i-47f187c1a2c3
        imp_type options:
            'gain': average gain of the feature when it is used in trees.
            'cover': average number of splits weighted by the number of samples concerned in each split a.k.a.
                     Gini Importance a.k.a. Mean Decrease in Impurity.
            'permutation': Permutation Importance or Mean Decrease in Accuracy
        """

        if len(self.feat_imp) == 0:
            self.get_importance()
        if std_kwarg:
            std = self.feat_imp_std[imp_type]
            pd.Series(self.feat_imp[imp_type]).plot(kind='barh',
                                                    title='Feature Importances ({0})'.format(imp_type),
                                                    xerr=pd.Series(std), ax=ax)
        else:
            pd.Series(self.feat_imp[imp_type]).plot(kind='barh',
                                         title='Feature Importances ({0})'.format(imp_type),
                                         ax=ax)
        plt.tight_layout()


    def score(self, x_data, y_data, method=None):

        raise NotImplementedError


    def cv(self, x_data, y_data, fold=5, method=None):

        raise NotImplementedError


    def plot_cv(self, ax):

        assert(self.cv_result is not None)
        x = np.array(self.cv_result.index.values)
        ax.plot(x, self.cv_result.iloc[:, 0].values, color='red', label='test')
        ax.plot(x, self.cv_result.iloc[:, 2].values, color='blue', label='training')
        ax.set_title('Training-Test CV Error')
        ax.set_xlabel('n_estimators')
        y_label = next(iter(self.cv_result.columns)).split('-')[1]
        ax.set_ylabel(y_label)
        ax.legend()
        plt.tight_layout()


    def plot_scatter(self, x_data, y_data, ax, **kwargs):
        y_pred = self.predict(x_data)
        ax.scatter(y_data.values, y_pred, color='green', **kwargs)
        ax.set_title('Observed-Predicted')
        ax.set_xlabel('observed')
        ax.set_ylabel('predicted')
        plt.tight_layout()


    @staticmethod
    def get_mutual_information(x_data, y_data):

        return generate_feature_ranking(x_data, y_data, beta=0.7)


    def plot_example_tree(self, f_name, dpi=300, **kwargs):

        NotImplementedError


class Regressor(BoostingBase):


    def __init__(self, **params):

        super(Regressor, self).__init__(**params)
        self.model = xgb.XGBRegressor(**params)


    def train(self, x_data, y_data):

        self.x_data = x_data
        self.y_data = y_data
        params = self.model.get_params()
        n_estimators = params['n_estimators']
        xgtrain = xgb.DMatrix(self.x_data, label=self.y_data)
        self.model = xgb.train(params, xgtrain, num_boost_round=n_estimators)


    def get_estimators(self):

        trees = self.model.get_dump(with_stats=True)
        for tree in trees:
            yield tree


    def predict(self, x_data):

        assert(isinstance(x_data, pd.DataFrame))
        data = xgb.DMatrix(x_data)
        return self.model.predict(data)


    def partial_dependence(self, feat, grid_size=50):

        x = self.x_data.copy()
        u = x[feat].values
        nan_mask = np.isnan(u)
        u[nan_mask] = np.nanmean(u)
        grid = np.linspace(np.percentile(u, 5), np.percentile(u, 95), grid_size)
        y_pred = np.zeros(len(grid))
        for i, val in enumerate(grid):
            x[feat] = val
            y_pred[i] = np.average(self.predict(x))
        return grid, y_pred


    def plot_partial_dependence(self, feat, ax, **kwargs):

        grid, y_pred = self.feat_dep[feat]
        ax.plot(grid, y_pred, '-', color='green', label='fit', **kwargs)
        ax.set_title(feat)
        ax.set_xlim(grid[0], grid[-1])
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles, labels, loc='best', fontsize=12)


    def get_default_score(self):

        return 'variance'


    def score(self, x_data, y_data, method='variance', **kwargs):
        """
        method options:
        'variance': Explained variance regression score function
        'mse': Mean squared error regression loss
        'r2': R^2 (coefficient of determination) regression score function.
        """

        assert(isinstance(x_data, pd.DataFrame))
        assert(isinstance(y_data, pd.Series))
        y_pred = self.predict(x_data)
        if method == 'variance':
            return explained_variance_score(y_data, y_pred, **kwargs)
        elif method == 'mse':
            return mean_squared_error(y_data, y_pred, **kwargs)
        elif method == 'r2':
            return r2_score(y_data, y_pred, **kwargs)


    def cv(self, fold=5, method='rmse'):
        """method option 'rmse': rooted mean square error"""

        params = {k: v for k, v in self.params.items() if v is not None}
        xgtrain = xgb.DMatrix(self.x_data, label=self.y_data)
        self.cv_result = xgb.cv(params,
                                xgtrain,
                                num_boost_round=params['n_estimators'],
                                metrics=method,
                                nfold=fold)


    def plot_scatter(self, ax, **kwargs):
        y_pred = self.predict(self.x_data)
        ax.scatter(self.y_data.values, y_pred, color='green', **kwargs)
        ax.set_title('Observed-Predicted')
        ax.set_xlabel('observed')
        ax.set_ylabel('predicted')
        plt.tight_layout()


    def plot_example_tree(self, f_name, dpi=300, **kwargs):
        model = xgb.XGBRegressor(**self.params)
        model.fit(self.x_data, self.y_data)
        fig, axes = plt.subplots()
        xgb.plot_tree(model, **kwargs, ax=axes)
        plt.savefig(f_name, dpi=dpi)


class Classifier(BoostingBase):


    def __init__(self, **params):

        super(Classifier, self).__init__(**params)
        self.model = xgb.XGBClassifier(**params)


    def train(self, x_data, y_data):

        self.x_data = x_data
        self.y_data = y_data
        self.model.fit(self.x_data, self.y_data)


    def predict(self, x_data):

        assert(isinstance(x_data, pd.DataFrame))
        return self.model.predict(x_data)


    def predict_proba(self, x_data):

        assert(isinstance(x_data, pd.DataFrame))
        return self.model.predict_proba(x_data)


    def partial_dependence(self, feat, grid_size=50):

        x = self.x_data.copy()
        u = x[feat].values
        nan_mask = np.isnan(u)
        u[nan_mask] = np.nanmean(u)
        grid = np.linspace(np.percentile(u, 5), np.percentile(u, 95), grid_size)
        y_pred = {}
        for i, val in enumerate(grid):
            x[feat] = val
            y_pred[i] = self.predict_proba(x)
        return grid, y_pred


    def plot_partial_dependence(self, feat, ax, **kwargs):

        grid, y_pred = self.feat_dep[feat]
        pred_dict = {i: list(map(lambda x: np.average(y_pred[i][:, x]),
                                 range(len(self.model.classes_)))) for i, a in enumerate(grid)}
        for k, val in enumerate(self.model.classes_):
            y = [pred_dict[i][k] for i, v in enumerate(grid)]
            ax.plot(grid, y, '-', color=color_list[k], label=str(val), **kwargs)
        ax.set_title(feat)
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles, labels, loc='best', fontsize=12)


    def get_estimators(self):

        trees = self.model.get_booster().get_dump(with_stats=True)
        for tree in trees:
            yield tree


    def get_default_score(self):

        return 'accuracy'


    def score(self, x_data, y_data, method='accuracy', **kwargs):
        """method options: 'accuracy', 'matthews'"""

        assert(isinstance(x_data, pd.DataFrame))
        assert(isinstance(y_data, pd.Series))
        y_pred = self.predict(x_data)
        if method == 'accuracy':
            return accuracy_score(y_data, y_pred, **kwargs)
        elif method == 'matthews':
            return matthews_corrcoef(y_data, y_pred, **kwargs)


    def cv(self, fold=5, method='merror'):
        """method option 'merror': exact matching error, used to evaluate multi-class classification"""

        params = self.model.get_params()
        params = {k: v for k, v in params.items() if v is not None}
        params['num_class'] = len(self.model.classes_)
        xgtrain = xgb.DMatrix(self.x_data, label=self.y_data)
        self.cv_result = xgb.cv(params,
                                xgtrain,
                                num_boost_round=params['n_estimators'],
                                metrics=method,
                                nfold=fold)


    def plot_example_tree(self, f_name, dpi=300, **kwargs):

        model = self.model
        fig, axes = plt.subplots()
        xgb.plot_tree(model, **kwargs, ax=axes)
        plt.savefig(f_name, dpi=dpi)


if __name__ == '__main__':
    pass
