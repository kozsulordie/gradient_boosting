README
======

My name is **gradient_boosting**. I am a Python package.

I wrap up functions within the reach of xgboost and scikit-learn to provide a comprehensive regression analysis
based on the machine learning techinique of Gradient Boosting. 

Install
-------

Clone with git

.. code::

   $ git clone git@bitbucket.org:ferran_muinos/gradient_boosting.git

or 

download from the `downloads page <https://bitbucket.org/ferran_muinos/gradient_boosting/downloads/>`_.

Move to the top folder of the repo and install with pip:

.. code::
	
	$ pip install .


Repo tree
---------

The repo has the following structure::

   |- logistic_enrichment/
   |
   |  |- logistic_enrichment/
   |  |  |
   |  |  |- __init__.py
   |  |  |- main.py
   |  |  |- methods.py
   |  |  |- mutual_information.py
   |
   |- example_notebook.ipynb
   |- .gitignore
   |- LICENSE
   |- MANIFEST.in
   |- README.rst
   |- requirements.txt
   |- setup.py


Usage example
-------------

The notebook `example_notebook.ipynb <http://nbviewer.jupyter.org/urls/bitbucket.org/ferran_muinos/gradient_boosting/raw/master/logistic_notebook.ipynb>`_ 
provides a standard example of usage.
